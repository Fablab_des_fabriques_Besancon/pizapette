#!/usr/bin/python
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import subprocess
import logging

# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
# on met le niveau du logger à DEBUG, comme ça il écrit tout
logger.setLevel(logging.DEBUG)
# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
file_handler = logging.FileHandler('/home/pi/exploit/mqtt-sub.log', 'w')
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

MQTT_SERVER = 'localhost'
MQTT_PATH = 'perf'

# Connexion au serveur MQTT et souscription au topic.
def on_connect(client, userdata, flags, rc):
    print("Connecté au serveur MQTT : " +(MQTT_SERVER))
    logging.info("Connecté au serveur MQTT : " +(MQTT_SERVER))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(MQTT_PATH)
    print("Souscription au topic : " +(MQTT_PATH) +" OK")
    logging.info("Souscription au topic : " +(MQTT_PATH) +" OK")

# Quand un message est reçu :
def on_message(client, userdata, msg):
    message = str(msg.payload)
    print("Message reçu : " +(message))
    # more callbacks, etc
    if message == 'bouton1' :
        subprocess.Popen('/bin/zik01', shell=True)
        logging.info("Message reçu : bouton1 > on lance zik01")
    elif message == 'bouton2' :
        subprocess.Popen('/bin/zik02', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton2 > on lance zik02")
    elif message == 'bouton3' :
        subprocess.Popen('/bin/vid01', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton3 > on lance vid01")
    elif message == 'bouton4' :
        subprocess.Popen('/bin/zik03', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton4 > on lance zik03")
    elif message == 'bouton5' :
        subprocess.Popen('/bin/diapoall', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton5 > on lance le diapo de toutes les photos")
    elif message == 'bouton6' :
        subprocess.Popen('/bin/killphoto', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton6 > on ferme la photo ou le diapo photo")
    elif message == 'bouton7' :
        subprocess.Popen('/bin/end', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton7 > on eteind la zik, la video ou les photos")
    elif message == 'bouton8' :
        subprocess.Popen('/bin/pause', shell=True, stdout=subprocess.PIPE)
        logging.info("Message reçu : bouton8 > on met la zik ou la video en pause")
    elif message == 'stop' :
        logging.info("Message reçu : stop > on eteind le raspi")
        client.disconnect() # on deconnecte proprement le client mqtt
        subprocess.Popen('sudo halt', shell=True, stdout=subprocess.PIPE)
        exit()

client = mqtt.Client()

client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTT_SERVER, 1883, 60)

client.loop_forever()
