#include <ESP8266WiFi.h>
#include <PubSubClient.h>


// Pin des boutons

const int bouton1 = 2;
const int bouton2 = 0;
const int bouton3 = 14;
const int bouton4 = 5;
const int bouton5 = 16;
const int bouton6 = 12;
const int bouton7 = 4;
const int bouton8 = A0;
const int bouton0 = 13;

int etatbouton1 = HIGH;
int etatbouton2 = HIGH;
int etatbouton3 = HIGH;
int etatbouton4 = HIGH;
int etatbouton5 = HIGH;
int etatbouton6 = HIGH;
int etatbouton7 = HIGH;
int etatbouton8 = 1024;
int etatbouton0 = HIGH;

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 500;    // the debounce time; increase if the output flickers


// Constantes Wifi & MQTT

const char* ssid = "MYSSID";
const char* password = "";
const char* mqtt_server = "192.168.1.254";

WiFiClient espClient;
PubSubClient client(espClient);


void setup_wifi() {

  delay(10);
  // Connexion Wifi
  Serial.println();
  Serial.print("Connection à ");
  Serial.print(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connecté");
  Serial.print("IP : ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Boucle de connexion MQTT
  while (!client.connected()) {
    Serial.print("Connexion MQTT...");
    // Créé un client aléatoire
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Tentative de connexion
    if (client.connect(clientId.c_str())) {
      Serial.println("connecté");
      temoin();
      // Une fois connecté on envoie un message...
      Serial.println("Envoi du message de connexion");
      client.publish("perf", "Bonjour, me voilà !");
    } else {
      Serial.print("oups, connexion ratée, rc=");
      Serial.print(client.state());
      Serial.println(" prochain essai dans 5 secondes");
      // attente de 5s avant prochaine tentative
      delay(5000);
    }
  }
}

void temoin() {
  pinMode(BUILTIN_LED, OUTPUT);
  for (int i = 0 ; i < 9 ; i++)
  { digitalWrite(BUILTIN_LED, LOW);
    delay(100);
    digitalWrite(BUILTIN_LED, HIGH);
    delay(100);
  }
}

void setup() {
  Serial.begin(115200);

  pinMode(bouton0, INPUT_PULLUP);
  pinMode(bouton1, INPUT_PULLUP);
  pinMode(bouton2, INPUT_PULLUP);
  pinMode(bouton3, INPUT_PULLUP);
  pinMode(bouton4, INPUT_PULLUP);
  pinMode(bouton5, INPUT_PULLUP);
  pinMode(bouton6, INPUT_PULLUP);
  pinMode(bouton7, INPUT_PULLUP);
  pinMode(bouton8, OUTPUT);

  setup_wifi(); //Établit la connexion Wifi
  client.setServer(mqtt_server, 1883); //Établit la connexion au serveur MQTT
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  delay(10); //Évite le pbm déconnexion wifi si A0 utilisé en analogRead
  etatbouton0 = digitalRead(bouton0);
  etatbouton1 = digitalRead(bouton1);
  etatbouton2 = digitalRead(bouton2);
  etatbouton3 = digitalRead(bouton3);
  etatbouton4 = digitalRead(bouton4);
  etatbouton5 = digitalRead(bouton5);
  etatbouton6 = digitalRead(bouton6);
  etatbouton7 = digitalRead(bouton7);
  etatbouton8 = analogRead(bouton8);

  //filtrage debounce
  if ( (millis() - lastDebounceTime) > debounceDelay) {

    //boucle bouton1
    if ( (etatbouton1 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton1 appuyé");
      client.publish("perf", "bouton1"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton2
    if ( (etatbouton2 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton2 appuyé");
      client.publish("perf", "bouton2"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton3
    if ( (etatbouton3 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton3 appuyé");
      client.publish("perf", "bouton3"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton4
    if ( (etatbouton4 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton4 appuyé");
      client.publish("perf", "bouton4"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton5
    if ( (etatbouton5 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton5 appuyé");
      client.publish("perf", "bouton5"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton6
    if ( (etatbouton6 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton6 appuyé");
      client.publish("perf", "bouton6"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }

    //boucle bouton7
    if ( (etatbouton7 == LOW & etatbouton0 == LOW) ) {
      Serial.println("bouton7 appuyé");
      client.publish("perf", "bouton7"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time
    }
    //boucle bouton8
    if ( (etatbouton8 < 200 & etatbouton0 == LOW) ) {
      Serial.println("bouton8 appuyé");
      client.publish("perf", "bouton8"); //On envoie le message au broker
      Serial.println("message envoyé au broker");
      lastDebounceTime = millis(); //set the current time

     //boucle stop
     if ( (etatbouton0 == LOW & etatbouton7 == LOW & etatbouton8 < 200) ) {
       Serial.println("stop demandé");
       client.publish("perf", "stop"); //On envoie le message au broker
       Serial.println("message envoyé au broker");
       lastDebounceTime = millis(); //set the current time
     }
    }
  }
}
